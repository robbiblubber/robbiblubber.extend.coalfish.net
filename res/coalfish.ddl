
CREATE TABLE USERS
(
	USERNAME		VARCHAR(96)		NOT NULL,
	HACTIVE			INTEGER			NOT NULL,
	HLEVEL			INTEGER			NOT NULL,
	HFLAGS			INTEGER			NOT NULL,
	EMAIL			TEXT			NOT NULL,
	NAME			VARCHAR(96)		NOT NULL,
	DESCR			TEXT,
	PASSWORD		VARCHAR(256),
	DOMAIN_NAME		VARCHAR(96),
	DOMAIN_USER		VARCHAR(96),
	CONSTRAINT PK_USERS PRIMARY KEY (USERNAME)
);



CREATE TABLE DATA
(
	ID				VARCHAR(96)		NOT NULL,
	HTYPE			INTEGER			NOT NULL,
	HALERT			INTEGER			NOT NULL,
	HCHECK			INTEGER			NOT NULL,
	HFLAGS			INTEGER			NOT NULL,
	URL				TEXT			NOT NULL,
	DESCR			TEXT,
	EMAIL			TEXT,
	EXPIRES			TIMESTAMP		NOT NULL,
	CHECKED			TIMESTAMP,
	JCREATED		VARCHAR(96),
	JMODIFIED		VARCHAR(96),
	JTIME			TIMESTAMP,
	CONSTRAINT PK_DATA PRIMARY KEY (ID)
);
