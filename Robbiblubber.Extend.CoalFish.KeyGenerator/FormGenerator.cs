﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Library.Coding;
using Robbiblubber.Util.SQL.SQLite;



namespace Robbiblubber.Extend.CoalFish.KeyGenerator
{
    public partial class FormGenerator: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public FormGenerator()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Path changed.</summary>
        private void _TextPath_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if(File.Exists(_TextPath.Text))
                {
                    SQLiteProvider p = new SQLiteProvider();
                    IDbCommand cmd = null;
                    bool ok = true;

                    try
                    {
                        p.FileName = _TextPath.Text;
                        p.Connect();

                        cmd = p.CreateCommand("SELECT VALUE FROM DATA WHERE KEY = 'db'");
                        ok = (((string) cmd.ExecuteScalar()) == "coalfish");
                    }
                    catch(Exception) { ok = false; }

                    Disposal.Dispose(p, cmd);

                    if(ok)
                    {
                        _TextKey.Text = p.DataKey;
                        _ButtonCopy.Enabled = true;
                        return;
                    }
                }
            }
            catch(Exception) { }

            _TextKey.Text = "";
            _ButtonCopy.Enabled = false;
        }


        /// <summary>Button "Copy" click.</summary>
        private void _ButtonCopy_Click(object sender, EventArgs e)
        {
            _TextKey.SelectAll();
            _TextKey.Copy();
        }


        /// <summary>Button "Browse" click.</summary>
        private void _ButtonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "CoalFish Database (coalfish.db)|coalfish.db|All Files (*.*)|*.*";

            if(f.ShowDialog() == DialogResult.OK)
            {
                _TextPath.Text = f.FileName;
            }
        }


        /// <summary>Button "Close" click.</summary>
        private void _ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
