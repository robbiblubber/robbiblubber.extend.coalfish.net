﻿namespace Robbiblubber.Extend.CoalFish.KeyGenerator
{
    partial class FormGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGenerator));
            this._LabelPath = new System.Windows.Forms.Label();
            this._TextPath = new System.Windows.Forms.TextBox();
            this._ButtonBrowse = new System.Windows.Forms.Button();
            this._ButtonCopy = new System.Windows.Forms.Button();
            this._TextKey = new System.Windows.Forms.TextBox();
            this._LabelKey = new System.Windows.Forms.Label();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._ButtonClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _LabelPath
            // 
            this._LabelPath.AutoSize = true;
            this._LabelPath.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPath.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelPath.Location = new System.Drawing.Point(40, 36);
            this._LabelPath.Name = "_LabelPath";
            this._LabelPath.Size = new System.Drawing.Size(84, 13);
            this._LabelPath.TabIndex = 0;
            this._LabelPath.Text = "Database &Path:";
            // 
            // _TextPath
            // 
            this._TextPath.Location = new System.Drawing.Point(43, 52);
            this._TextPath.Name = "_TextPath";
            this._TextPath.Size = new System.Drawing.Size(505, 25);
            this._TextPath.TabIndex = 0;
            this._TextPath.TextChanged += new System.EventHandler(this._TextPath_TextChanged);
            // 
            // _ButtonBrowse
            // 
            this._ButtonBrowse.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonBrowse.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonBrowse.Image")));
            this._ButtonBrowse.Location = new System.Drawing.Point(554, 52);
            this._ButtonBrowse.Name = "_ButtonBrowse";
            this._ButtonBrowse.Size = new System.Drawing.Size(25, 25);
            this._ButtonBrowse.TabIndex = 0;
            this._ButtonBrowse.TabStop = false;
            this._ToolTip.SetToolTip(this._ButtonBrowse, "Browse");
            this._ButtonBrowse.UseVisualStyleBackColor = true;
            this._ButtonBrowse.Click += new System.EventHandler(this._ButtonBrowse_Click);
            // 
            // _ButtonCopy
            // 
            this._ButtonCopy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCopy.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonCopy.Image")));
            this._ButtonCopy.Location = new System.Drawing.Point(554, 110);
            this._ButtonCopy.Name = "_ButtonCopy";
            this._ButtonCopy.Size = new System.Drawing.Size(25, 25);
            this._ButtonCopy.TabIndex = 1;
            this._ButtonCopy.TabStop = false;
            this._ToolTip.SetToolTip(this._ButtonCopy, "Copy to Clipboard");
            this._ButtonCopy.UseVisualStyleBackColor = true;
            this._ButtonCopy.Click += new System.EventHandler(this._ButtonCopy_Click);
            // 
            // _TextKey
            // 
            this._TextKey.BackColor = System.Drawing.SystemColors.Window;
            this._TextKey.Location = new System.Drawing.Point(43, 110);
            this._TextKey.Name = "_TextKey";
            this._TextKey.ReadOnly = true;
            this._TextKey.Size = new System.Drawing.Size(505, 25);
            this._TextKey.TabIndex = 1;
            // 
            // _LabelKey
            // 
            this._LabelKey.AutoSize = true;
            this._LabelKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelKey.Location = new System.Drawing.Point(40, 94);
            this._LabelKey.Name = "_LabelKey";
            this._LabelKey.Size = new System.Drawing.Size(27, 13);
            this._LabelKey.TabIndex = 1;
            this._LabelKey.Text = "&Key:";
            // 
            // _ButtonClose
            // 
            this._ButtonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonClose.Location = new System.Drawing.Point(554, 218);
            this._ButtonClose.Name = "_ButtonClose";
            this._ButtonClose.Size = new System.Drawing.Size(25, 25);
            this._ButtonClose.TabIndex = 2;
            this._ButtonClose.TabStop = false;
            this._ButtonClose.UseVisualStyleBackColor = true;
            this._ButtonClose.Click += new System.EventHandler(this._ButtonClose_Click);
            // 
            // FormGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonClose;
            this.ClientSize = new System.Drawing.Size(629, 203);
            this.Controls.Add(this._ButtonClose);
            this.Controls.Add(this._ButtonCopy);
            this.Controls.Add(this._TextKey);
            this.Controls.Add(this._LabelKey);
            this.Controls.Add(this._ButtonBrowse);
            this.Controls.Add(this._TextPath);
            this.Controls.Add(this._LabelPath);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "FormGenerator";
            this.Text = "CoalFish Key Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelPath;
        private System.Windows.Forms.TextBox _TextPath;
        private System.Windows.Forms.Button _ButtonBrowse;
        private System.Windows.Forms.Button _ButtonCopy;
        private System.Windows.Forms.TextBox _TextKey;
        private System.Windows.Forms.Label _LabelKey;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Button _ButtonClose;
    }
}

