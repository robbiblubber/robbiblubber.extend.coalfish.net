﻿using System;



namespace Robbiblubber.Extend.CoalFish.Struct
{
    /// <summary>This class defines user access levels.</summary>
    public sealed class AccessLevel
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This describes a user permission with no access.</summary>
        public readonly AccessLevel NONE = new AccessLevel(0);
        
        /// <summary>This describes a user permission with read only access.</summary>
        public readonly AccessLevel READ = new AccessLevel(1);
        
        /// <summary>This describes a user permission with read/write access.</summary>
        public readonly AccessLevel WRITE = new AccessLevel(2);

        /// <summary>This describes a user permission with full administrative access.</summary>
        public readonly AccessLevel ADMIN = new AccessLevel(9);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="level">Level.</param>
        private AccessLevel(int level)
        {
            Level = level;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the numeric access level.</summary>
        public int Level { get; private set; }


        /// <summary>Gets the access level name.</summary>
        public string Name
        {
            get
            {
                switch(Level)
                {
                    case 1: return "read only";
                    case 2: return "read/write";
                    case 9: return "admin";
                }
                return "none";
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // operators                                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if two instances are equal.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if the objects are equal, otherwise returns FALSE.</returns>
        public static bool operator ==(AccessLevel a, object b)
        {
            if(((object) a) == null) { return (b == null); }
            return a.Equals(b);
        }


        /// <summary>Returns if two instances are equal.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if the objects are equal, otherwise returns FALSE.</returns>
        public static bool operator ==(object a, AccessLevel b)
        {
            if(((object) b) == null) { return (a == null); }
            return b.Equals(a);
        }


        /// <summary>Returns if two instances are not equal.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if the objects are not equal, otherwise returns FALSE.</returns>
        public static bool operator !=(AccessLevel a, object b)
        {
            if(((object) a) == null) { return (b != null); }
            return (!a.Equals(b));
        }


        /// <summary>Returns if two instances are not equal.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if the objects are not equal, otherwise returns FALSE.</returns>
        public static bool operator !=(object a, AccessLevel b)
        {
            if(((object) b) == null) { return (a != null); }
            return (!b.Equals(a));
        }


        /// <summary>Return if an instance is greater than an object.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if a is greater than b, otherwise returns FALSE.</returns>
        public static bool operator >(AccessLevel a, object b)
        {
            if((((object) a) == null) || (b == null)) { return false; }
            if(b is int) { return (a.Level > (int) b); }
            if(b is AccessLevel) { return (a.Level > ((AccessLevel) b).Level); }

            return false;
        }


        /// <summary>Return if an instance is greater than an object.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if a is greater than b, otherwise returns FALSE.</returns>
        public static bool operator >(object a, AccessLevel b)
        {
            if((((object) b) == null) || ((object) b == null)) { return false; }
            if(a is int) { return ((int) a > b.Level); }
            if(b is AccessLevel) { return (((AccessLevel) a).Level > b.Level); }

            return false;
        }


        /// <summary>Return if an instance is less than an object.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if a is less than b, otherwise returns FALSE.</returns>
        public static bool operator <(AccessLevel a, object b)
        {
            if((((object) a) == null) || (b == null)) { return false; }
            if(b is int) { return (a.Level < (int) b); }
            if(b is AccessLevel) { return (a.Level < ((AccessLevel) b).Level); }

            return false;
        }


        /// <summary>Return if an instance is less than an object.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if a is less than b, otherwise returns FALSE.</returns>
        public static bool operator <(object a, AccessLevel b)
        {
            if((((object) b) == null) || ((object) b == null)) { return false; }
            if(a is int) { return ((int) a < b.Level); }
            if(b is AccessLevel) { return (((AccessLevel) a).Level < b.Level); }

            return false;
        }


        /// <summary>Return if an instance is greater or equal than an object.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if a is greater or equal than b, otherwise returns FALSE.</returns>
        public static bool operator >=(AccessLevel a, object b)
        {
            if((((object) a) == null) || (b == null)) { return false; }
            if(b is int) { return (a.Level >= (int) b); }
            if(b is AccessLevel) { return (a.Level >= ((AccessLevel) b).Level); }

            return false;
        }


        /// <summary>Return if an instance is greater or equal than an object.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if a is greater or equal than b, otherwise returns FALSE.</returns>
        public static bool operator >=(object a, AccessLevel b)
        {
            if((((object) b) == null) || ((object) b == null)) { return false; }
            if(a is int) { return ((int) a >= b.Level); }
            if(b is AccessLevel) { return (((AccessLevel) a).Level >= b.Level); }

            return false;
        }


        /// <summary>Return if an instance is less or equal than an object.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if a is less or equal than b, otherwise returns FALSE.</returns>
        public static bool operator <=(AccessLevel a, object b)
        {
            if((((object) a) == null) || (b == null)) { return false; }
            if(b is int) { return (a.Level <= (int) b); }
            if(b is AccessLevel) { return (a.Level <= ((AccessLevel) b).Level); }

            return false;
        }


        /// <summary>Return if an instance is less or equal than an object.</summary>
        /// <param name="a">Object.</param>
        /// <param name="b">Object.</param>
        /// <returns>Returns TRUE if a is less or equal than b, otherwise returns FALSE.</returns>
        public static bool operator <=(object a, AccessLevel b)
        {
            if((((object) b) == null) || ((object) b == null)) { return false; }
            if(a is int) { return ((int) a <= b.Level); }
            if(b is AccessLevel) { return (((AccessLevel) a).Level <= b.Level); }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return Name;
        }


        /// <summary>Returns if the object equals an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Returns TRUE if the objects are equal, otherwise returns FALSE.</returns>
        public override bool Equals(object obj)
        {
            if(obj == null) return false;
            if(obj is int) { return (Level == (int) obj); }
            if(obj is AccessLevel) { return (Level == ((AccessLevel) obj).Level); }
            
            return false;
        }


        /// <summary>Returns the hash code for this instance.</summary>
        /// <returns>Hash code.</returns>
        public override int GetHashCode()
        {
            return Level.GetHashCode();
        }
    }
}
