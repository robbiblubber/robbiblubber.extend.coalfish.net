﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Extend.CoalFish.Struct
{
    /// <summary>This class provides data access.</summary>
    public static class Data
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database provider.</summary>
        private static IProvider _InternalProvider = null;


        /// <summary>Schema.</summary>
        private static string _Schema = "";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static properties                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database connection.</summary>
        private static IProvider _Provider
        {
            get
            {
                if(_InternalProvider == null)
                {
                    if(!Directory.Exists(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\CoalFish")) { Directory.CreateDirectory(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\CoalFish"); }
                    if(File.Exists(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\CoalFish\coalfish.xps5"))
                    {
                        Connect(File.ReadAllText(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\CoalFish\coalfish.xps5"));
                    }
                }

                return _InternalProvider;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if a provider is required.</summary>
        public static bool ProviderRequired
        {
            get { return (_Provider == null); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provides a database file.</summary>
        /// <param name="database">Key.</param>
        /// <returns>Returns TRUE if the database file is valid, otherwise returns FALSE.</returns>
        public static bool Connect(string key)
        {
            IProvider p = null;
            string schema = null;
            bool ok = false;

            try
            {
                if((p = SQLProvider.GetProvider(key)) == null) return false;
                p.Connect();

                List<string> schemata = new List<string>();
                schemata.Add("");
                foreach(string i in Ddp.Load(PathOp.ApplicationDirectory + @"\coalfish.config").GetStringArray("schema", "coalfish"))
                {
                    if(string.IsNullOrWhiteSpace(i)) continue;
                    if(!schemata.Contains(i + ".")) { schemata.Add(i + "."); }
                }

                IDbCommand cmd = null;
                foreach(string i in schemata)
                {
                    try
                    {
                        cmd = p.CreateCommand("SELECT VALUE FROM " + i + "DATA WHERE KEY = 'db'");
                        if(ok = (((string) cmd.ExecuteScalar()) == "coalfish"))
                        {
                            cmd.Dispose();
                            schema = i; break;
                        }
                    }
                    catch(Exception) {}

                    cmd.Dispose();
                }
            }
            catch(Exception) {}

            if(ok)
            {
                try
                {
                    if(_InternalProvider != null) { _InternalProvider.Disconnect(); }
                }
                catch(Exception) {}

                File.WriteAllText(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\CoalFish\coalfish.xps5", key);

                _InternalProvider = p;
                _Schema = schema;

                return true;
            }
            else
            {
                try{ p.Disconnect(); } catch(Exception) {}                
                return false;
            }
        }
    }
}
