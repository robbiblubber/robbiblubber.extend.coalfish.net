﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using Robbiblubber.Extend.CoalFish.Struct;
using Robbiblubber.Util.Library;
using Robbiblubber.Util.Library.Coding;
using Robbiblubber.Util.SQL.SQLite;



namespace Robbiblubber.Extend.CoalFish
{
    /// <summary>This class implements the connection window.</summary>
    public partial class FormConnection: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Cancel close flag.</summary>
        private bool _Cancel = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormConnection()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the connection has been successful.</summary>
        public bool Success
        {
            get; private set;
        } = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Button "Paste" click.</summary>
        private void _ButtonPaste_Click(object sender, EventArgs e)
        {
            _TextKey.SelectAll();
            _TextKey.Paste();
        }


        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Success = false;
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            Success = Data.Connect(_TextKey.Text);
            
            if(!Success)
            {
                _Cancel = (MessageBox.Show("The connection key is invalid.", "Failed to Connect", MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK);
            }

            if(!_Cancel) Close();
        }


        /// <summary>Form closing.</summary>
        private void FormConnection_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = _Cancel;
            _Cancel = false;
        }
    }
}
