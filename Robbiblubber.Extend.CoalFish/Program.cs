﻿using Robbiblubber.Extend.CoalFish.Struct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace Robbiblubber.Extend.CoalFish
{
    /// <summary>This provides the main program class.</summary>
    internal static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Main window.</summary>
        private static FormMain _Main = null;

        /// <summary>Splash window.</summary>
        private static FormSplash _Splash = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // entry point                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>The main entry point for the application.</summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            _Splash = new FormSplash();
            _Splash.Show();
            Application.DoEvents();

            if(Data.ProviderRequired)
            {
                FormConnection f = new FormConnection();
                f.ShowDialog();

                if(!f.Success) { return; }

            }
            else { Thread.Sleep(600); }

            _Main = new FormMain();
            _Splash.DelayClose();

            Application.Run(_Main);
        }
    }
}
