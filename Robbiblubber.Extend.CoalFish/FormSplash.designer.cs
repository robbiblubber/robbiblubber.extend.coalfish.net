﻿namespace Robbiblubber.Extend.CoalFish
{
    partial class FormSplash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSplash));
            this._LabelVersion = new System.Windows.Forms.Label();
            this._LabelName = new System.Windows.Forms.Label();
            this._LabelRobbiblubber = new System.Windows.Forms.Label();
            this._LabelIcon = new System.Windows.Forms.Label();
            this._TimeHide = new System.Windows.Forms.Timer(this.components);
            this._PanelFrame = new System.Windows.Forms.Panel();
            this._PanelFrame.SuspendLayout();
            this.SuspendLayout();
            // 
            // _LabelVersion
            // 
            this._LabelVersion.AutoSize = true;
            this._LabelVersion.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelVersion.Location = new System.Drawing.Point(335, 270);
            this._LabelVersion.Name = "_LabelVersion";
            this._LabelVersion.Size = new System.Drawing.Size(104, 21);
            this._LabelVersion.TabIndex = 12;
            this._LabelVersion.Text = "Version 1.0.0";
            // 
            // _LabelName
            // 
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.Image = ((System.Drawing.Image)(resources.GetObject("_LabelName.Image")));
            this._LabelName.Location = new System.Drawing.Point(308, 232);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(305, 40);
            this._LabelName.TabIndex = 11;
            // 
            // _LabelRobbiblubber
            // 
            this._LabelRobbiblubber.Image = ((System.Drawing.Image)(resources.GetObject("_LabelRobbiblubber.Image")));
            this._LabelRobbiblubber.Location = new System.Drawing.Point(453, 30);
            this._LabelRobbiblubber.Name = "_LabelRobbiblubber";
            this._LabelRobbiblubber.Size = new System.Drawing.Size(213, 30);
            this._LabelRobbiblubber.TabIndex = 10;
            // 
            // _LabelIcon
            // 
            this._LabelIcon.Image = ((System.Drawing.Image)(resources.GetObject("_LabelIcon.Image")));
            this._LabelIcon.Location = new System.Drawing.Point(34, 43);
            this._LabelIcon.Name = "_LabelIcon";
            this._LabelIcon.Size = new System.Drawing.Size(257, 257);
            this._LabelIcon.TabIndex = 9;
            // 
            // _TimeHide
            // 
            this._TimeHide.Interval = 400;
            this._TimeHide.Tick += new System.EventHandler(this._TimeHide_Tick);
            // 
            // _PanelFrame
            // 
            this._PanelFrame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelFrame.Controls.Add(this._LabelName);
            this._PanelFrame.Dock = System.Windows.Forms.DockStyle.Fill;
            this._PanelFrame.ForeColor = System.Drawing.Color.Black;
            this._PanelFrame.Location = new System.Drawing.Point(0, 0);
            this._PanelFrame.Name = "_PanelFrame";
            this._PanelFrame.Size = new System.Drawing.Size(700, 340);
            this._PanelFrame.TabIndex = 13;
            // 
            // FormSplash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(246)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(700, 340);
            this.ControlBox = false;
            this.Controls.Add(this._LabelVersion);
            this.Controls.Add(this._LabelRobbiblubber);
            this.Controls.Add(this._LabelIcon);
            this.Controls.Add(this._PanelFrame);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSplash";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CoalFish";
            this._PanelFrame.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelVersion;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.Label _LabelRobbiblubber;
        private System.Windows.Forms.Label _LabelIcon;
        private System.Windows.Forms.Timer _TimeHide;
        private System.Windows.Forms.Panel _PanelFrame;
    }
}