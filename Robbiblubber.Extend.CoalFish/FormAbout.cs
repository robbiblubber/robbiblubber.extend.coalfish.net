﻿using System.Windows.Forms;

using Robbiblubber.Util.Library;



namespace Robbiblubber.Extend.CoalFish
{
    /// <summary>This class implements the about window.</summary>
    internal partial class FormAbout: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormAbout()
        {
            InitializeComponent();
            _LabelVersion.Text = "Version " + VersionOp.ApplicationVersion.ToVersionString();
        }
    }
}
