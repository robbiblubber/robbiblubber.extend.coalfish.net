﻿namespace Robbiblubber.Extend.CoalFish
{
    partial class FormConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConnection));
            this._ButtonPaste = new System.Windows.Forms.Button();
            this._TextKey = new System.Windows.Forms.TextBox();
            this._LabelKey = new System.Windows.Forms.Label();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._ButtonOK = new System.Windows.Forms.Button();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _ButtonPaste
            // 
            this._ButtonPaste.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonPaste.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonPaste.Image")));
            this._ButtonPaste.Location = new System.Drawing.Point(554, 52);
            this._ButtonPaste.Name = "_ButtonPaste";
            this._ButtonPaste.Size = new System.Drawing.Size(25, 25);
            this._ButtonPaste.TabIndex = 0;
            this._ButtonPaste.TabStop = false;
            this._ToolTip.SetToolTip(this._ButtonPaste, "Paste from Clipboard");
            this._ButtonPaste.UseVisualStyleBackColor = true;
            this._ButtonPaste.Click += new System.EventHandler(this._ButtonPaste_Click);
            // 
            // _TextKey
            // 
            this._TextKey.BackColor = System.Drawing.SystemColors.Window;
            this._TextKey.Location = new System.Drawing.Point(43, 52);
            this._TextKey.Name = "_TextKey";
            this._TextKey.Size = new System.Drawing.Size(505, 25);
            this._TextKey.TabIndex = 0;
            // 
            // _LabelKey
            // 
            this._LabelKey.AutoSize = true;
            this._LabelKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelKey.Location = new System.Drawing.Point(40, 36);
            this._LabelKey.Name = "_LabelKey";
            this._LabelKey.Size = new System.Drawing.Size(120, 13);
            this._LabelKey.TabIndex = 0;
            this._LabelKey.Text = "Enter Connection &Key:";
            // 
            // _ButtonOK
            // 
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(231, 120);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(168, 34);
            this._ButtonOK.TabIndex = 1;
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(411, 120);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(168, 34);
            this._ButtonCancel.TabIndex = 2;
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // FormConnection
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(629, 203);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._ButtonPaste);
            this.Controls.Add(this._TextKey);
            this.Controls.Add(this._LabelKey);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConnection";
            this.Text = "Connect to Database";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormConnection_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button _ButtonPaste;
        private System.Windows.Forms.TextBox _TextKey;
        private System.Windows.Forms.Label _LabelKey;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.Button _ButtonCancel;
    }
}

