﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Library;



namespace Robbiblubber.Extend.CoalFish
{
    /// <summary>Splash form.</summary>
    internal partial class FormSplash: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormSplash()
        {
            InitializeComponent();

            _LabelVersion.Text = "Version " + VersionOp.ApplicationVersion.ToVersionString();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Waits some time before closing the window.</summary>
        public void DelayClose()
        {
            _TimeHide.Enabled = true;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Timer event handler.</summary>
        private void _TimeHide_Tick(object sender, EventArgs e)
        {
            _TimeHide.Enabled = false;
            Close();
        }
    }
}
